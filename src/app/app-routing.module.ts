import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { StudentRegisterComponent } from './auth/student-register/student-register.component';
import { HomeComponent } from './teacher/home/home.component';
import { LocationComponent } from './teacher/location/location.component';

const routes: Routes = [
  { path: '', component: RegisterComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'student/register', component: StudentRegisterComponent },
  { path: 'teacher/dashboard', component: HomeComponent },
  { path: 'teacher/location', component: LocationComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
