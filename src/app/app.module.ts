import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { StudentRegisterComponent } from './auth/student-register/student-register.component';
import { HomeComponent } from './teacher/home/home.component';
import { SideNavigationComponent } from './components/side-navigation/side-navigation.component';
import { LocationComponent } from './teacher/location/location.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    StudentRegisterComponent,
    HomeComponent,
    SideNavigationComponent,
    LocationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
