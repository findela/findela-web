import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor() { }

  autoComplete = true;

  @ViewChild('time') time: ElementRef;
  @ViewChild('autocompleteText') autocompleteText: ElementRef;

  ngOnInit(): void {
    $(() => {
      $('.some-time-inputs').timepicker(
        {
          disableTextInput: true,
          minTime: '5:00 AM',
          maxTime: '11:30 PM',
          timeFormat: 'g:i A'
        }
        );
    });
  }

  getTimeValue(){
    const time = this.time.nativeElement.value;
    console.log(new Date(), time);
  }

  autoCompleteText() {
    if (this.autocompleteText.nativeElement.value.length > 3) {
      this.autoComplete = true;
    }
  }

}
